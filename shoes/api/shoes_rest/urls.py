from django.urls import path
from .views import api_shoe_detail, api_shoes

urlpatterns = [
    path("shoes/", api_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoe_detail, name="api_shoe_detail"),
]
