from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from django.http import JsonResponse

from common.json import ModelEncoder
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "name"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "styleName"]

    def get_extra_data(self, o):
        return {"location": o.location.name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "styleName",
        "color",
        "pictureURL",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }



# Create your views here.
@require_http_methods(["GET", "POST"]) ## create views using http request methods GET and POST 
def api_list_hats(request):
    ## GET Request;
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    ## POST Request:
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, pk):
    if request.method == "GET":
        shoe = Hat.objects.all(id=pk)
        return JsonResponse(
            shoe,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_show_hats(request, pk):
#     if request.method == "GET":
#         try:
#             hat = Hat.objects.get(id=pk)
#             return JsonResponse(
#                 hat,
#                 encoder=HatListEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             response = JsonResponse(
#                 {"message": "Doesn't exist!"}
#             )
#             response.status_code=404
#             return response
#     elif request.method == "DELETE":
#         try:
#             hat = Hat.objects.get(id=pk)
#             hat.delete()
#             return JsonResponse(
#                 hat,
#                 encoder=HatListEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Doesn't exist!"}
#             )
#     else:
#         try:
#             content = json.loads(request.body)
#             location = Hat.objects.get(id=pk)
#             props = [
#                 "fabric",
#                 "styleName",
#                 "color",
#                 "pictureURL",
#                 "location"
#             ]

#             for prop in props:
#                 if prop in content:
#                     setattr(hat, prop, content[prop])
#             hat.save()
#             return JsonResponse(
#                 hat,
#                 encoder=HatListEncoder,
#                 safe=False
#             )
#         except Hat.DoesNotExist:
#             response = JsonResponse(
#                 {"message": "Doesn't exist!"}
#             )
#             response.status_code=404
#             return response

