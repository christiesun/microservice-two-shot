import React from "react";

class ShoesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturer: "",
      model_name: "",
      color: "",
      url: "",
      bins: [],
    };

    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handleURLChange = this.handleURLChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBinChange = this.handleBinChange.bind(this);
  }

  handleBinChange(event) {
    const value = event.target.value;
    this.setState({ bin: value });
  }

  handleURLChange(event) {
    const value = event.target.value;
    this.setState({ url: value });
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleModelNameChange(event) {
    const value = event.target.value;
    this.setState({ model_name: value });
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({ manufacturer: value });
  }

  async handleSubmit(event) {
    event.preventDefault();

    const data = { ...this.state };
    console.log("data1:", data);
    delete data.bins;
    console.log("data:", data);

    const shoesUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoes = await response.json();
      console.log(newShoes);
    }
    const cleared = {
      manufacturer: "",
      model_name: "",
      color: "",
      url: "",
      bin: "",
    };
    this.setState(cleared);
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/bins/";

    const response = await fetch(url);
    console.log("response ok");
    if (response.ok) {
      const data = await response.json();
      console.log("data:", data);
      this.setState({ bins: data.bins });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new pair of shoes</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleManufacturerChange}
                  placeholder="manufacturer"
                  value={this.state.manufacturer}
                  required
                  type="text"
                  name="manufacturer"
                  id="nmanufacturer"
                  className="form-control"
                />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleModelNameChange}
                  placeholder="model name"
                  value={this.state.mmodel_name}
                  required
                  type="text"
                  name="model_name"
                  id="model_name"
                  className="form-control"
                />
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleColorChange}
                  placeholder="color"
                  value={this.state.color}
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleURLChange}
                  placeholder="url"
                  value={this.state.url}
                  required
                  type="text"
                  name="url"
                  id="url"
                  className="form-control"
                />
                <label htmlFor="url">URL</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleBinChange}
                  required
                  name="bins"
                  id="bins"
                  className="form-select"
                >
                  <option value="">Choose a bin</option>
                  {this.state.bins.map((bin) => {
                    return (
                      <option key={bin.href} value={bin.href}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoesForm;
