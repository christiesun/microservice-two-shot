import { NavLink } from "react-router-dom";
import React, { useEffect, useState } from "react";

function ShoesList(props) {
  const [shoes, setShoes] = useState([]);

  async function fetchData() {
    const response = await fetch("http://localhost:8080/api/shoes/");

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    } else {
      console.log("Error fetching data.");
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const removeData = (id) => {
    fetch(`http://localhost:8080/api/shoes/${id}`, { method: "DELETE" }).then(
      () => fetchData()
    );
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Model Name</th>
            <th>Bin</th>
            <th>Delete item</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe, i) => {
            return (
              <tr key={i}>
                <td>{shoe.model_name}</td>
                <td>{shoe.bin}</td>
                <td>
                  <button onClick={() => removeData(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button>
        <NavLink className="nav-link" to="/shoes/new">
          Create New
        </NavLink>
      </button>
    </div>
  );
}
export default ShoesList;
