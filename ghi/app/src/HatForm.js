import React from 'react';

class HatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fabric: '',
      styleName: '',
      color: '',
      pictureURL: '',
      location: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeFabric = this.handleChangeFabric.bind(this);
    this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangePictureURL = this.handleChangePictureURL.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
  }

  handleChangeFabric(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }

  handleChangeStyleName(event) {
    const value = event.target.value;
    this.setState({ styleName: value });
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }

  handleChangePictureURL(event) {
    const value = event.target.value;
    this.setState({ pictureURL: value });
  }

  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }


  async handleSubmit(event){
    event.preventDefault();
    const data = {...this.state};
    data.picture_url = data.picture;
    delete data.picture;
    delete data.locations;
    console.log(data);

    const postUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(postUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log("New Hat: ", newHat);
    }

    const cleared = {
      fabric: '',
      style: '',
      color: '',
      picture:'',
      location: '',
    };

    this.setState(cleared);


  }


// to select the locations
async componentDidMount() {
  const url = 'http://localhost:8100/api/locations/';

  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();

    this.setState({locations: data.locations});

  }
}

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={this.handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleFabricChange} placeholder="Fabric" value={this.state.fabric} required type="text" id="fabric" name="fabric" className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStyleChange} placeholder="Style" value={this.state.style} required type="text" id="style" name="style" className="form-control"/>
                <label htmlFor="style">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="Color" value={this.state.color} required type="text" id="color" name="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureChange} placeholder="Picture" value={this.state.picture} required type="url" id="picture" name="picture" className="form-control"/>
                <label htmlFor="picture">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.location.map(location => {
                    return (
                      <option key={location.href} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
        </div>

    );
  }
}

export default HatForm;