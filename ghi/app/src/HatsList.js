import { NavLink } from "react-router-dom";
import React from "react";


function HatsList(props) { //
    return (
      <div>
        <button>
          <NavLink className="nav-link" to="/hats/new">
            Create a hat
          </NavLink>
        </button>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style Name</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
              return (
                <tr key={hat.href}>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.styleName }</td>
                  <td>{ hat.color }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
  
  export default HatsList;